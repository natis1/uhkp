namespace UHKP
{
    public static class globals
    {
        public const int SETTINGS_VER = 2;
        public const string MOD_VERSION_STRING = "0.0.2";
        public const string SETTINGS_FILE_APPEND = "UHKP.settings.json";
        public const string MOD_NAME_FULL = "Unofficial Hollow Knight Patch";
        
        // We wanna load after modcommon but before almost all the other mods.
        // -90 should be good enough since modcommon loads as -100.
        public const int LOAD_PRIORITY = -90;

        // For other mods...
        public static readonly int MOD_VERSION_INT = 5;

        public static settings settingsFile;
    }
}