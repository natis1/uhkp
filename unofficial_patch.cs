﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Modding;
using UnityEngine;
using Logger = Modding.Logger;

namespace UHKP
{
    public class unofficial_patch : Mod, ITogglableMod
    {
        private readonly string settingsFilename;
        private settings globalSettings = Activator.CreateInstance<settings>();

        private void saveGlobalSettings()
        {
            log("Saving combomod settings!");
            if (File.Exists(settingsFilename + ".bak"))
                File.Delete(settingsFilename + ".bak");
            if (File.Exists(settingsFilename))
                File.Move(settingsFilename, settingsFilename + ".bak");
            using (FileStream fileStream = File.Create(settingsFilename))
            {
                using (StreamWriter streamWriter = new StreamWriter((Stream) fileStream))
                {
                    string json = JsonUtility.ToJson((object) globalSettings, true);
                    streamWriter.Write(json);
                }
            }
        }
        
        private void loadGlobalSettings()
        {
            log("Loading combomod settings!");
            if (!File.Exists(settingsFilename))
                return;
            using (FileStream fileStream = File.OpenRead(settingsFilename))
            {
                using (StreamReader streamReader = new StreamReader((Stream) fileStream))
                    globalSettings = JsonUtility.FromJson<settings>(streamReader.ReadToEnd());
            }
        }
        
        private void setupSettings()
        {
            string settingsFilePath = Application.persistentDataPath + ModHooks.PathSeperator + globals.SETTINGS_FILE_APPEND;
            bool forceReloadGlobalSettings = (globalSettings != null && globalSettings.settingsVersion != globals.SETTINGS_VER);
            if (forceReloadGlobalSettings || !File.Exists(settingsFilePath))
            {
                if (forceReloadGlobalSettings)
                {
                    log("Settings outdated! Rebuilding.");
                }
                else
                {
                    log("Settings not found, rebuilding... File will be saved to: " + settingsFilePath);
                }

                globalSettings?.reset();
            }
            
            saveGlobalSettings();
            globals.settingsFile = globalSettings;
        }

        
        public unofficial_patch()
        {
            settingsFilename = Application.persistentDataPath + ModHooks.PathSeperator + globals.SETTINGS_FILE_APPEND;

            loadGlobalSettings();
            
            FieldInfo field = typeof(Mod).GetField
                ("Name", BindingFlags.Instance | BindingFlags.Public);
            field?.SetValue(this, globals.MOD_NAME_FULL);
        }

        public override string GetVersion()
        {
            string ver = globals.MOD_VERSION_STRING;
            // put version of modding api you are building against here. Probably 44.
            int minAPI = 40;
            bool apiTooLow = Convert.ToInt32(ModHooks.Instance.ModVersion.Split('-')[1]) < minAPI;
            
            bool noModCommon = true;
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in assemblies)
            {
                try
                {
                    if (assembly.GetTypes().All(type => type.Namespace != "ModCommon")) continue;
                    noModCommon = false;
                    break;
                }
                catch
                {
                    Log(assembly.FullName + " failed to load.");
                }
            }
            if (noModCommon) ver += " (Error: UHKP requires ModCommon)";
            
            
            if (apiTooLow) ver += " (Error: ModAPI too old)";
            log("Version is " + ver);
            return ver;
        }
        
        public override void Initialize()
        {
            setupSettings();
            ModHooks.Instance.AfterSavegameLoadHook += saveGame;
            ModHooks.Instance.NewGameHook += addComponent;
            ModHooks.Instance.ApplicationQuitHook += saveGlobalSettings;
        }

        private static void saveGame(SaveGameData data)
        {
            addComponent();
        }

        private static void addComponent()
        {
            // add components here
            
            GameManager.instance.gameObject.AddComponent<dash_fixes>();

            if (globals.settingsFile.assetGraphicalFix)
            {
                load_assets.loadAllTextures();
                GameManager.instance.gameObject.AddComponent<sprite_fixes>();
            }

            if (globals.settingsFile.healthFixes)
            {
                GameManager.instance.gameObject.AddComponent<health_fixes>();
            }
        }

        public override int LoadPriority()
        {
            return globals.LOAD_PRIORITY;
        }

        public void Unload()
        {
            ModHooks.Instance.AfterSavegameLoadHook -= saveGame;
            ModHooks.Instance.NewGameHook -= addComponent;
        }
        
        public static void log(string text)
        {
            Logger.Log( "[" + globals.MOD_NAME_FULL + "] " + text);
        }
    }
}