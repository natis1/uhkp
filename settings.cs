using Modding;

namespace UHKP
{
    public class settings : IModSettings
    {
        public void reset()
        {
            BoolValues.Clear();
            IntValues.Clear();
            FloatValues.Clear();
            StringValues.Clear();

            settingsVersion = globals.SETTINGS_VER;

            sharpShadowTinkFix = true;
            assetGraphicalFix = true;
            healthFixes = true;
        }
        public int settingsVersion { get => GetInt(); private set => SetInt(value); }

        public bool sharpShadowTinkFix { get => GetBool(); private set => SetBool(value); }
        public bool assetGraphicalFix { get => GetBool(); private set => SetBool(value); }
        public bool healthFixes { get => GetBool(); private set => SetBool(value); }

        
    }
}