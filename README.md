# UHKP - 0.0.2

The Unofficial Hollow Knight Patch fixes problems with the base game. Current patches which exist for it include:

### Dash Fixes

Fixes the sharp shadow tink bug. Tinking an enemy with sharp shadow will no longer cause sharp shadow to stop dealing damage until you reset your game.

### Sprite Fixes

Fixes some of the worse looking sprites in the game using sprite art from FoldingPapers. Currently just fixes the Hollow Knight statue in the Hall of Gods.

### Health Fixes

Radiant enemies will always kill you, even if you would otherwise get lucky with Carefree Melody. Includes Radiant Infinite Grimm.

Taking more damage than lifeblood masks can protect against no longer causes the damage to completely ignore the lifeblood.

These were both 1 line fixes that should have been in vanilla.

## More fixes coming soon

Why not request fixes in the issues section on gitlab?
