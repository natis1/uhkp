using System.Collections;
using GlobalEnums;
using ModCommon.Util;
using Modding;
using UnityEngine;

namespace UHKP
{
    public class health_fixes : MonoBehaviour
    {
        private static bool canTakeDamage(int hazardType)
        {
            return (HeroController.instance.damageMode != DamageMode.NO_DAMAGE &&
                    HeroController.instance.transitionState == HeroTransitionState.WAITING_TO_TRANSITION &&
                    (!HeroController.instance.cState.invulnerable && !HeroController.instance.cState.recoiling) &&
                    (!HeroController.instance.playerData.isInvincible && !HeroController.instance.cState.dead &&
                     (!HeroController.instance.cState.hazardDeath && !BossSceneController.IsTransitioning)) &&
                    (HeroController.instance.damageMode != DamageMode.HAZARD_ONLY || hazardType != 1) &&
                    (!HeroController.instance.cState.shadowDashing || hazardType != 1) &&
                    ((double) HeroController.instance.parryInvulnTimer <= 0.0 || hazardType != 1));
        }
        
        private int takeDamage(ref int hazardType, int damage)
        {
            if (!canTakeDamage(hazardType))
                return damage;
            
            
            
            // Radiant enemies will oneshot the player regardless of if they have carefree on or not.
            if (damage >= 50 && HeroController.instance.carefreeShieldEquipped && hazardType == 1)
            {
                HeroController.instance.SetAttr<int>("hitsSinceShielded", 0);
            }
            
            return damage;
        }
        
        private void newTakeHealth(On.PlayerData.orig_TakeHealth orig, PlayerData self, int amount)
        {
            if (amount > 0 && self.health == self.maxHealth && self.health != self.CurrentMaxHealth)
            {
                self.health = self.CurrentMaxHealth;
            }
            if (self.healthBlue > 0)
            {
                int num = amount - self.healthBlue;
                self.damagedBlue = true; 
                self.healthBlue -= amount;
                
                /*
                 * Commenting this out causes the player to not take 3 dmg when they take 2 dmg and they have
                 * 1 mask of lifeblood
                 *
                 * Otherwise you take 1 dmg from self.healthBlue -= amount;
                 * 1 dmg from this function
                 * and 1 dmg from the bottom function
                 * 
                 
                if (self.healthBlue < 0)
                {
                    self.health += self.healthBlue;
                } */
                if (num > 0)
                {
                    self.TakeHealth(num);
                }
            }
            else
            {
                self.damagedBlue = false;
                if (self.health - amount <= 0)
                {
                    self.health = 0;
                }
                else
                {
                    self.health -= amount;
                }
            }
        }
        
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(2f);
            ModHooks.Instance.TakeDamageHook += takeDamage;
            On.PlayerData.TakeHealth += newTakeHealth;
        }



        private void OnDestroy()
        {
            ModHooks.Instance.TakeDamageHook -= takeDamage;
            On.PlayerData.TakeHealth -= newTakeHealth;
        }
    }
}