using System.Collections;
using Modding;
using UnityEngine;

namespace UHKP
{
    public class dash_fixes : MonoBehaviour
    {
        public PlayMakerFSM sharpShadowFSM;
        private bool firstShadeDash = true;


        private void Start()
        {
            StartCoroutine(setupFields());

            if (globals.settingsFile.sharpShadowTinkFix)
            {
                ModHooks.Instance.DashPressedHook += sharpShadowFix;
            }
        }

        private void OnDestroy()
        {
            if (globals.settingsFile.sharpShadowTinkFix)
            {
                ModHooks.Instance.DashPressedHook -= sharpShadowFix;
            }
        }
        
        private IEnumerator setupFields()
        {
            while (HeroController.instance == null)
                yield return null;
            
            foreach (GameObject go in Resources.FindObjectsOfTypeAll<GameObject>())
            {
                if (go == null || !go.CompareTag("Sharp Shadow")) continue;
                    
                sharpShadowFSM = FSMUtility.LocateFSM(go, "damages_enemy");
                unofficial_patch.log($@"Found Sharp Shadow: {go} - {sharpShadowFSM}.");
            }
        }
        
        
        
        
        /*
         * In the base game, tinking with sharp shadow breaks it forever, causing it to no longer do damage until
         * you reload from a save. UHKP can fix this.
         */
        

        private bool sharpShadowFix()
        {
            // Fixes TC problem where after tink sharp shadow is broken
            if (sharpShadowFSM != null && !firstShadeDash)
            {
                sharpShadowFSM.SetState("Idle");
            } else if (sharpShadowFSM != null)
            {
                firstShadeDash = false;
            }

            return false;
        }
        
        
        
    }
}